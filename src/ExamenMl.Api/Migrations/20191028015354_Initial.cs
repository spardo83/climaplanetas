﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ExamenMl.Api.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Planetas",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Nombre = table.Column<string>(nullable: true),
                    DesplazamientoPorDia = table.Column<double>(nullable: false),
                    GradosDesplazadosPorSegundo = table.Column<double>(nullable: false),
                    DistanciaAlSol = table.Column<int>(nullable: false),
                    GradosActual = table.Column<double>(nullable: false),
                    EnumValue = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Planetas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VitacoraClimaticas",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Dia = table.Column<long>(nullable: false),
                    Lluvia = table.Column<bool>(nullable: false),
                    Sequia = table.Column<bool>(nullable: false),
                    Optimo = table.Column<bool>(nullable: false),
                    Indeterminado = table.Column<bool>(nullable: false),
                    PicoLluvias = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VitacoraClimaticas", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Planetas");

            migrationBuilder.DropTable(
                name: "VitacoraClimaticas");
        }
    }
}
