using System;
using System.Threading.Tasks;
using ExamenMl.Servicios.Contracts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;

namespace ExamenMl.Api.Quartz
{
    public class CalcularDiaJobIJob : IJob
    {
        private readonly ILogger<CalcularDiaJobIJob> _log;
        private readonly IServiceProvider _provider;

        public CalcularDiaJobIJob(ILogger<CalcularDiaJobIJob> log, IServiceProvider provider)
        {
            _log = log;
            _provider = provider;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            _log.LogInformation("Ejecutando tarea de calculo dia extra");

            // Obtiene el scope de el inyector para luego obtener una instancia del servicio de bitacora
            using (var scope = _provider.CreateScope())
            {
                var vitacoraClimaticaServicio = scope.ServiceProvider.GetService<IBitacoraClimaticaServicio>();
                try
                {
                    await vitacoraClimaticaServicio.CalcularDia();
                }
                catch (Exception e)
                {
                    _log.LogError(e,"Error en el job que calcula dias subsiguientes");
                }
                
            }
        }

        
    }
}