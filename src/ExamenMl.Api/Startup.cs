using System.Reflection;
using ExamenMl.Api.Configuration;
using ExamenMl.Dominio.Context;
using ExamenMl.Servicios.Hubs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Core.Enrichers;
using Serilog.Enrichers.AspNetCore.HttpContext;

namespace ExamenMl.Api
{
    public class Startup
    {
        private readonly string _migrationsAssembly;

        public Startup(IConfiguration configuration)
        {
            _migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContext<GalaxiaDbContext>(options =>
                    options
                        .UseSqlite(Configuration.GetConnectionString("DbConnection"),
                            sql => { sql.MigrationsAssembly(_migrationsAssembly); })
                )
                .AddCors(options => options.AddPolicy("CorsPolicy",
                    builder =>
                    {
                        builder.AllowAnyMethod()
                            .AllowAnyHeader()
                            .WithOrigins(Configuration.GetGeneralConfiguration().AllowedOrigins)
                            .AllowCredentials();
                    }))
                .AddSwaggerConfiguration()
                .Inject()
                .UseQuartz()
                .AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
      

            app.UseSerilogLogContext(options =>
            {
                options.EnrichersForContextFactory = context => new[]
                {
                    new PropertyEnricher("TraceIdentifier", context.TraceIdentifier) 
                };
            });
            app.UseSerilogRequestLogging();
            
            app.Seed();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.BuildSwagger()
                .UseRouting()
                .UseRouting()
                .UseCors("CorsPolicy")
                .UseAuthorization()
                .UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                    endpoints.MapHub<MovimientoHub>("/movimiento");
                });
        }
    }
}