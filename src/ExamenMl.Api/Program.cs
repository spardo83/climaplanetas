using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Enrichers.AspNetCore;
using Serilog.Events;
using Serilog.Exceptions;

namespace ExamenMl.Api
{
    public class Program
    {
        //Deshabilita el warning que genera el XM de comentarios
#pragma warning disable CS1591
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); })
                .UseSerilog((context, configuration) =>
                {
                    configuration.MinimumLevel.Debug()
                        .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                        .Enrich.WithExceptionDetails().Enrich.FromLogContext()
                        .WriteTo.File("climaplanetas-error.log", LogEventLevel.Error)
                        .WriteTo.File("climaplanetas-warn.log", LogEventLevel.Warning)
                        .WriteTo.File("climaplanetas-info.log", LogEventLevel.Information)
                        .WriteTo.Console(LogEventLevel.Debug);
                });
    }
#pragma warning restore CS1591
}