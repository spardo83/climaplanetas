using ExamenMl.Dominio.Entidades;

namespace ExamenMl.Api.Dto.Extensions
{
    public static class DiaDtoExtension
    {
        /// <summary>
        /// Mapea una entidad VitacoraClimatica a un DiaDto
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static DiaDto Map(this VitacoraClimatica entity)
        {
            var diaDto = new DiaDto
            {
                Dia = entity.Dia,
                Determinado = true,
                Lluvias = entity.Lluvia ? "Si" : "No",
                Sequia = entity.Sequia ? "Si" : "No",
                Optimo = entity.Optimo ? "Si" : "No"
            };
            if (!entity.Lluvia && !entity.Sequia && !entity.Optimo)
            {
                diaDto.Determinado = false;
            }

            return diaDto;
        }
    }
}