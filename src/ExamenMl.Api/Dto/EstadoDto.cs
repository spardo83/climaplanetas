namespace ExamenMl.Api.Dto
{
    /// <summary>
    /// Dto que transforma el estado en un objeto util
    /// </summary>
    public class EstadoDto
    {
        public string Mensaje { get; }
        public long DiasGenerados { get; }
        public long DiasTotales { get; }

        public EstadoDto(long diasGenerados, long diasTotales)
        {
            DiasGenerados = diasGenerados;
            DiasTotales = diasTotales;
            Mensaje =
                $"La tarea de generación de datos está en ejecución. Se generaron {diasGenerados} de {diasTotales}";
        }

        public EstadoDto()
        {
            Mensaje = "Los datos se estan eliminando, aguarde por favor";
        }
    }
}