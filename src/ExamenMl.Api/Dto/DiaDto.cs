namespace ExamenMl.Api.Dto
{
    /// <summary>
    /// Dto de los resultados del dia en cuestion
    /// </summary>
    public class DiaDto
    {
        public bool Determinado { get; set; }
        public string Lluvias { get; set; }
        public string Optimo { get; set; }
        public string Sequia { get; set; }
        public long Dia { get; set; }
    }
}