using Microsoft.Extensions.Configuration;

namespace ExamenMl.Api.Configuration
{
    public static class ConfigurationExtensions
    {
        /// <summary>
        /// Devuelve un objeto de configuracion general obtenido de app.settings.json
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static GeneralConfiguration GetGeneralConfiguration(this IConfiguration configuration)
        {
            var sectionClientConfiguration = configuration.GetSection("GeneralConfiguration");
            return sectionClientConfiguration.Get<GeneralConfiguration>();
        }
        
    

    }
}