using ExamenMl.Api.Quartz;
using ExamenMl.Servicios.Contracts;
using ExamenMl.Servicios.Implementation;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;

namespace ExamenMl.Api.Configuration
{
    public static class DependencyInjection
    {
        /// <summary>
        /// Injecta las dependencias de la aplicacion
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection Inject(this IServiceCollection services)
        {
            // Se inyectan las dependencias por scope
            services.AddScoped<IBitacoraClimaticaServicio, BitacoraClimaticaServicio>();
            services.AddScoped<IPlanetaServicio, PlanetaServicio>();
            // Estadoservice debe inyectarse como singleton para poder cumplir su función
            services.AddSingleton<IEstadoService, EstadoService>();
            return services;
        }
        
        /// <summary>
        /// Configura quartz
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection UseQuartz(this IServiceCollection services)
        {

            services.AddSingleton<IJobFactory, SingletonJobFactory>();
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();

            services.AddSingleton<CalcularDiaJobIJob>();
            //Inyecta el jobSchedule como singleton para ejecutarse todos los dias a las 00:00 horas del servidor
            services.AddSingleton(new JobSchedule(
                jobType: typeof(CalcularDiaJobIJob),
                cronExpression: "0 0 0 * * ?"));
            
            services.AddHostedService<QuartzHostedService>();
            return services;
        }
    }
}