using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace ExamenMl.Api.Configuration
{
    public static class SwaggerConfigExtensions
    {
        /// <summary>
        /// Inyeccion de swagger, utilizando los comentarios del XML generado
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddSwaggerConfiguration(this IServiceCollection services)
        {
            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "Calificaciones API", Version = "v1"});
                c.IncludeXmlComments(xmlPath);
                c.EnableAnnotations();
            });
            return services;
        }

        /// <summary>
        /// Configura el uso de Swagger y Swagger UI
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder BuildSwagger(this IApplicationBuilder app)
        {
            app.UseSwagger()
                .UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Planetas API V1");
                    c.DocumentTitle = "Planetas API";
                });
            return app;
        }
    }
}