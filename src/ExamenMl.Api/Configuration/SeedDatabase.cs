using System;
using ExamenMl.Dominio.Context;
using ExamenMl.Servicios.Contracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;

namespace ExamenMl.Api.Configuration
{
    public static class SeedDatabase
    {
        public static void Seed(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                // En esta etapa no fue inyectado el logger, es por eso que se utiliza el estatico
                var logger = Log.Logger;
                // Ejecuta las migrations para actualizar la base de datos
                var context = serviceScope.ServiceProvider.GetRequiredService<GalaxiaDbContext>();
                try
                {
                    context.Database.Migrate();
                }
                catch (Exception e)
                {
                    logger.Error(e,"Error durante la ejecucion de migraciones");
                }
                

                // Inicializa los datos de los planetas
                var planetaService = serviceScope.ServiceProvider.GetRequiredService<IPlanetaServicio>();
                try
                {
                    planetaService.CreatePlanetasAsync();
                }
                catch (Exception e)
                {
                    logger.Error(e,"Error durante el seed inicial");
                }
                
            }
        }
    }
}