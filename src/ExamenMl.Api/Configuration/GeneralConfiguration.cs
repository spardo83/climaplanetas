namespace ExamenMl.Api.Configuration
{
    /// <summary>
    /// Modelo de configuración general, debe ser igual al json en app.settings
    /// </summary>
    public class GeneralConfiguration
    {
        public string[] AllowedOrigins { get; set; }
    }
}