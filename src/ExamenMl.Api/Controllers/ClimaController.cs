﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ExamenMl.Api.Dto;
using ExamenMl.Api.Dto.Extensions;
using ExamenMl.Servicios.Contracts;
using ExamenMl.Servicios.Dto;
using ExamenMl.Servicios.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;

namespace ExamenMl.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ClimaController : ControllerBase
    {
        private readonly IBitacoraClimaticaServicio _bitacoraClimaticaServicio;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IEstadoService _estadoService;
        private readonly ILogger<ClimaController> _logger;

        public ClimaController(IBitacoraClimaticaServicio bitacoraClimaticaServicio,
            IServiceScopeFactory serviceScopeFactory, IEstadoService estadoService, ILogger<ClimaController> logger)
        {
            _bitacoraClimaticaServicio = bitacoraClimaticaServicio;
            _serviceScopeFactory = serviceScopeFactory;
            _estadoService = estadoService;
            _logger = logger;
        }

        /// <summary>
        /// Genera los datos climaticos de los años solicitados.
        /// En caso de estar en proceso la generación de datos devuelve un mensaje advirtiendo esto con los datos del avance
        /// </summary>
        /// <param name="anios">Años a generar</param>
        /// <param name="tipoDesplazamiento">Tipo de desplazamiento (0 para dias, 1 para segundos , 2 milisegundos)</param>
        /// <returns></returns>
        [HttpPost("generar/{anios}/{tipoDesplazamiento}")]
        [SwaggerResponse(200, "Mensaje de inicio exitoso", typeof(string))]
        [SwaggerResponse(200, "Mensaje trabajo en proceso", typeof(EstadoDto))]
        public IActionResult GenerarDatos(int anios, TipoDesplazamiento tipoDesplazamiento)
        {

            if (_estadoService.EstaEliminando())
            {
                _logger.LogInformation("Se estan eliminando los registros, se cancela la solicitud");
                return Ok(new EstadoDto());
            }

            if (_estadoService.EstaGenerando())
            {
                _logger.LogInformation("Se estan generando los registros, se cancela la solicitud");
                return Ok(new EstadoDto(_estadoService.GetDiasGenerados(), _estadoService.GetDiasTotales()));
            }

            Task.Run(async () =>
            {
                using var scope = _serviceScopeFactory.CreateScope();
                var vitacoraClimaticaServicio = scope.ServiceProvider.GetService<IBitacoraClimaticaServicio>();
                var logger = scope.ServiceProvider.GetService<ILogger>();
                try
                {
                    await vitacoraClimaticaServicio.GenerarAnios(anios, tipoDesplazamiento);
                }
                catch (Exception e)
                {
                    logger.LogError(e,"Fallo la tarea de generacion de datos");
                }
                
            });

            return Ok("La generación de los datos se disparo de forma correcta");
        }

        /// <summary>
        /// Comienza el movimiento de los planetas, el tipo de movimiento puede ser (0 dias, 1 segundos, 2 milisegundos)
        /// </summary>
        /// <returns></returns>
        [HttpGet("IniciarMovimiento/{tipoDesplazamiento}")]
        [SwaggerResponse(200, "Mensaje de inicio", typeof(string))]
        public IActionResult IniciarMovimiento(TipoDesplazamiento tipoDesplazamiento)
        {
            Task.Run(async () =>
            {
                // Se crea un scope para que la tarea continue corriendo en background y a la vez darle una respuesta
                // al cliente que consulta
                using var scope = _serviceScopeFactory.CreateScope();
                var vitacoraClimaticaServicio = scope.ServiceProvider.GetService<IBitacoraClimaticaServicio>();
                var logger = scope.ServiceProvider.GetService<ILogger>();
                try
                {
                    logger.LogInformation("Se inicio el movimiento de planetas");
                    await vitacoraClimaticaServicio.Movimiento(tipoDesplazamiento);
                }
                catch (Exception e)
                {
                    logger.LogError(e,"Error durante el movimiento de planetas");
                }
                
            });

            return Ok("El movimiento de los planetas comenzo");
        }

        /// <summary>
        /// Detiene el movimiento de los planetas
        /// </summary>
        /// <returns></returns>
        [HttpGet("DetenerMovimiento/{tipoDesplazamiento}")]
        [SwaggerResponse(200, "Mensaje de finalizacion", typeof(string))]
        public IActionResult DetenerMovimiento()
        {
            _estadoService.FinalizarMovimientoPlanetas();
            _logger.LogInformation("Finalizo el movimiento de planetas");
            return Ok("La generación de los datos se disparo de forma correcta");
        }

        /// <summary>
        /// Obtiene los periodos totales de lluvia,sequia,optimos e indeterminados totales
        /// </summary>
        /// <returns></returns>
        [HttpGet("periodos")]
        [SwaggerResponse(200, "Listado dto con los datos solicitados", typeof(List<ResultadosMeteorologicosDto>))]
        [SwaggerResponse(200, "Mensaje trabajo en proceso", typeof(EstadoDto))]
        public async Task<IActionResult> Periodos()
        {
            if (_estadoService.EstaEliminando())
            {
                _logger.LogInformation("Se estan eliminando los registros, se cancela la solicitud");
                return Ok(new EstadoDto());
            }

            if (_estadoService.EstaGenerando())
            {
                _logger.LogInformation("Se estan generando los registros, se cancela la solicitud");
                return Ok(new EstadoDto(_estadoService.GetDiasGenerados(), _estadoService.GetDiasTotales()));
            }

            return Ok(await _bitacoraClimaticaServicio.GetResultados());
        }

        /// <summary>
        ///  Elimina los datos generados
        /// </summary>
        /// <returns></returns>
        [HttpDelete("reset")]
        [SwaggerResponse(200, "Aviso de comienzo de eliminación", typeof(string))]
        [SwaggerResponse(200, "Mensaje trabajo en proceso", typeof(EstadoDto))]
        public IActionResult Reset()
        {
            if (_estadoService.EstaEliminando())
            {
                _logger.LogInformation("Se estan eliminando los registros, se cancela la solicitud");
                return Ok(new EstadoDto());
            }

            if (_estadoService.EstaGenerando())
            {
                _logger.LogInformation("Se estan generando los registros, se cancela la solicitud");
                return Ok(new EstadoDto(_estadoService.GetDiasGenerados(), _estadoService.GetDiasTotales()));
            }

            Task.Run(async () =>
            {
                using var scope = _serviceScopeFactory.CreateScope();
                var vitacoraClimaticaServicio = scope.ServiceProvider.GetService<IBitacoraClimaticaServicio>();
                var logger = scope.ServiceProvider.GetService<ILogger>();
                try
                {
                    logger.LogInformation("Comenzo la eliminación de registros");
                    await vitacoraClimaticaServicio.Reset();
                    logger.LogInformation("Finalizo la eliminación de registros");
                }
                catch (Exception e)
                {
                    logger.LogError(e,"Fallo la eliminación de registros");
                }
                
            });
            return Ok("Comenzo la eliminación de los datos");
        }

        /// <summary>
        /// Obtiene los datos de un dia particular
        /// </summary>
        /// <param name="numero">Numero de dia para obtener datos</param>
        /// <returns></returns>
        [HttpGet("dia/{numero}")]
        [SwaggerResponse(200, "Dto con los datos solicitados", typeof(DiaDto))]
        [SwaggerResponse(200, "Mensaje trabajo en proceso", typeof(EstadoDto))]
        public async Task<IActionResult> GetDia(long numero)
        {
            if (_estadoService.EstaEliminando())
            {
                _logger.LogInformation("Se estan eliminando los registros, se cancela la solicitud");
                return Ok(new EstadoDto());
            }

            if (_estadoService.EstaGenerando())
            {
                _logger.LogInformation("Se estan generando los registros, se cancela la solicitud");
                return Ok(new EstadoDto(_estadoService.GetDiasGenerados(), _estadoService.GetDiasTotales()));
            }

            var dia = await _bitacoraClimaticaServicio.GetDiaAsync(numero);
            if (dia == null) return NotFound("No existe el día en la bitacora");

            return Ok(dia.Map());
        }
    }
}