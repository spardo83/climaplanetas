using System;
using System.Drawing;
using ExamenMl.Servicios.Maths;
using Xunit;

namespace ExamenMl.Test
{
    public class MathExtensionsTest
    {
        [Fact]
        public void CalculoDePosicionPuntos()
        {
            double angulo = 0;

            var punto = angulo.CalcularPosicion(10);

            Assert.Equal(10, punto.X);
            Assert.Equal(0, punto.Y);
        }

        [Fact]
        public void PuntoPerteneceRecta()
        {
            var p1 = new Point(0, 0);
            var p2 = new Point(1, 1);
            var p3 = new Point(2, 2);

            var resultado = p2.PuntoPerteneceRecta(p1, p3);

            Assert.True(resultado);

            p1 = new Point(0, 0);
            p2 = new Point(1, 0);
            p3 = new Point(-1, 0);

            resultado = p2.PuntoPerteneceRecta(p1, p3);

            Assert.True(resultado);
        }

        [Fact]
        public void PuntoNoPerteneceRecta()
        {
            var p1 = new Point(0, 0);
            var p2 = new Point(2, 1);
            var p3 = new Point(2, 2);

            var resultado = p2.PuntoPerteneceRecta(p1, p3);

            Assert.False(resultado);

            p1 = new Point(0, 0);
            p2 = new Point(1, 1);
            p3 = new Point(-1, -2);

            resultado = p2.PuntoPerteneceRecta(p1, p3);

            Assert.False(resultado);
        }

        [Fact]
        public void CalculoPerimetroCorrecto()
        {
            var p1 = new Point(0, 0);
            var p2 = new Point(1, 1);
            var p3 = new Point(1, 0);

            var resultado = MathExtensions.Perimetro(p1, p2, p3);

            Assert.Equal(3, resultado);

            p1 = new Point(0, 0);
            p2 = new Point(-1, -1);
            p3 = new Point(0, -1);

            resultado = MathExtensions.Perimetro(p1, p2, p3);

            Assert.Equal(3, resultado);
        }

        [Fact]
        public void CalculoPerimetroIncorrecto()
        {
            var p1 = new Point(0, 0);
            var p2 = new Point(2, 2);
            var p3 = new Point(1, 0);

            var resultado = MathExtensions.Perimetro(p1, p2, p3);

            Assert.NotEqual(3, resultado);

            p1 = new Point(0, 0);
            p2 = new Point(-2, -2);
            p3 = new Point(0, -1);

            resultado = MathExtensions.Perimetro(p1, p2, p3);

            Assert.NotEqual(3, resultado);
        }

        [Fact]
        public void CalcularArea()
        {
            var p1 = new Point(0, 0);
            var p2 = new Point(2, 2);
            var p3 = new Point(2, 0);

            var resultado = MathExtensions.Area(p1, p2, p3);

            Assert.Equal(2, resultado);

            p1 = new Point(0, 0);
            p2 = new Point(-2, -2);
            p3 = new Point(0, -2);

            resultado = MathExtensions.Area(p1, p2, p3);

            Assert.Equal(2, resultado);
        }

        [Fact]
        public void CalculoAreaIncorrecto()
        {
            var p1 = new Point(0, 0);
            var p2 = new Point(4, 4);
            var p3 = new Point(4, 0);

            var resultado = MathExtensions.Area(p1, p2, p3);

            Assert.NotEqual(15, resultado);

            p1 = new Point(0, 0);
            p2 = new Point(-4, -4);
            p3 = new Point(0, -4);

            resultado = MathExtensions.Area(p1, p2, p3);

            Assert.NotEqual(15, resultado);
        }

        [Fact]
        public void PuntoDentroDeTriangulo()
        {
            var p1 = new Point(0, 0);
            var p2 = new Point(4, 4);
            var p3 = new Point(4, 0);
            var dentro = new Point(1, 1);

            var resultado = dentro.EstaDentroDelTriangulo(p1, p2, p3);

            Assert.True(resultado);
        }

        [Fact]
        public void PuntoFueraDeTriangulo()
        {
            var p1 = new Point(0, 0);
            var p2 = new Point(4, 4);
            var p3 = new Point(4, 0);
            var dentro = new Point(5, 4);

            var resultado = dentro.EstaDentroDelTriangulo(p1, p2, p3);

            Assert.False(resultado);
        }

        [Fact]
        public void SegundosPorDia()
        {
            Assert.Equal(86400, MathExtensions.SegundoPorDia(1));
            Assert.Equal(172800, MathExtensions.SegundoPorDia(2));
        }

        [Fact]
        public void SegundosPorHora()
        {
            Assert.Equal(3600, MathExtensions.SegundosPorHora(1));
            Assert.Equal(7200, MathExtensions.SegundosPorHora(2));
        }

        [Fact]
        public void DiasPorAnios()
        {
            Assert.Equal(365, MathExtensions.DiasPorAnios(1));
            Assert.Equal(2190, MathExtensions.DiasPorAnios(6));
        }
    }
}