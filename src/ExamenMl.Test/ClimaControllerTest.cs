using System.Threading.Tasks;
using ExamenMl.Api.Controllers;
using ExamenMl.Api.Dto;
using ExamenMl.Dominio.Entidades;
using ExamenMl.Servicios.Contracts;
using ExamenMl.Servicios.Dto;
using ExamenMl.Servicios.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace ExamenMl.Test
{
    public class ClimaControllerTest
    {
        [Fact]
        public async void RespuestaCorrectaEstadoGenerando()
        {
            var estadoServicio = new Mock<IEstadoService>();
            var vitacoraClimatica = new Mock<IBitacoraClimaticaServicio>();
            var serviceScopeFactory = new Mock<IServiceScopeFactory>();
            var logger = new Mock<ILogger<ClimaController>>();
            estadoServicio.Setup(s => s.EstaEliminando()).Returns(false);
            estadoServicio.Setup(s => s.EstaGenerando()).Returns(true);

            var climaController = new ClimaController(vitacoraClimatica.Object, serviceScopeFactory.Object,
                estadoServicio.Object, logger.Object);


            var resultGenerarDatos = climaController.GenerarDatos(1, TipoDesplazamiento.Dias);

            var responseGenerarDatos = Assert.IsType<OkObjectResult>(resultGenerarDatos);
            Assert.IsType<EstadoDto>(responseGenerarDatos.Value);

            var resultPeriodos = await climaController.Periodos();

            var responsePeriodos = Assert.IsType<OkObjectResult>(resultPeriodos);
            Assert.IsType<EstadoDto>(responsePeriodos.Value);

            var resultReset = climaController.Reset();

            var responseReset = Assert.IsType<OkObjectResult>(resultReset);
            Assert.IsType<EstadoDto>(responseReset.Value);

            var resultGetDia = await climaController.GetDia(1);

            var responseGetDia = Assert.IsType<OkObjectResult>(resultGetDia);
            Assert.IsType<EstadoDto>(responseGetDia.Value);
        }

        [Fact]
        public async void RespuestaCorrectaEstaEliminando()
        {
            var estadoServicio = new Mock<IEstadoService>();
            var vitacoraClimatica = new Mock<IBitacoraClimaticaServicio>();
            var serviceScopeFactory = new Mock<IServiceScopeFactory>();
            var logger = new Mock<ILogger<ClimaController>>();
            estadoServicio.Setup(s => s.EstaEliminando()).Returns(true);

            var climaController = new ClimaController(vitacoraClimatica.Object, serviceScopeFactory.Object,
                estadoServicio.Object, logger.Object);


            var resultGenerarDatos = climaController.GenerarDatos(1, TipoDesplazamiento.Dias);

            var responseGenerarDatos = Assert.IsType<OkObjectResult>(resultGenerarDatos);
            Assert.IsType<EstadoDto>(responseGenerarDatos.Value);

            var resultPeriodos = await climaController.Periodos();

            var responsePeriodos = Assert.IsType<OkObjectResult>(resultPeriodos);
            Assert.IsType<EstadoDto>(responsePeriodos.Value);

            var resultReset = climaController.Reset();

            var responseReset = Assert.IsType<OkObjectResult>(resultReset);
            Assert.IsType<EstadoDto>(responseReset.Value);

            var resultGetDia = await climaController.GetDia(1);

            var responseGetDia = Assert.IsType<OkObjectResult>(resultGetDia);
            Assert.IsType<EstadoDto>(responseGetDia.Value);
        }

        [Fact]
        public void InicioGeneracionDatos()
        {
            var estadoServicio = new Mock<IEstadoService>();
            var vitacoraClimatica = new Mock<IBitacoraClimaticaServicio>();
            var serviceScopeFactory = new Mock<IServiceScopeFactory>();
            var logger = new Mock<ILogger<ClimaController>>();
            estadoServicio.Setup(s => s.EstaGenerando()).Returns(false);
            estadoServicio.Setup(s => s.EstaEliminando()).Returns(false);
            vitacoraClimatica.Setup(v => v.GenerarAnios(1, TipoDesplazamiento.Dias))
                .ReturnsAsync(new ResultadosMeteorologicosDto());

            var climaController = new ClimaController(vitacoraClimatica.Object, serviceScopeFactory.Object,
                estadoServicio.Object, logger.Object);

            var result = climaController.GenerarDatos(1, TipoDesplazamiento.Dias);

            var response = Assert.IsType<OkObjectResult>(result);
            Assert.IsType<string>(response.Value);
        }

        [Fact]
        public async void GetPeriodos()
        {
            var estadoServicio = new Mock<IEstadoService>();
            var vitacoraClimatica = new Mock<IBitacoraClimaticaServicio>();
            var serviceScopeFactory = new Mock<IServiceScopeFactory>();
            var logger = new Mock<ILogger<ClimaController>>();
            estadoServicio.Setup(s => s.EstaGenerando()).Returns(false);
            estadoServicio.Setup(s => s.EstaEliminando()).Returns(false);
            vitacoraClimatica.Setup(v => v.GetResultados())
                .ReturnsAsync(new ResultadosMeteorologicosDto());

            var climaController = new ClimaController(vitacoraClimatica.Object, serviceScopeFactory.Object,
                estadoServicio.Object, logger.Object);

            var result = await climaController.Periodos();

            var response = Assert.IsType<OkObjectResult>(result);
            Assert.IsType<ResultadosMeteorologicosDto>(response.Value);
        }

        [Fact]
        public void InicioReset()
        {
            var estadoServicio = new Mock<IEstadoService>();
            var vitacoraClimatica = new Mock<IBitacoraClimaticaServicio>();
            var serviceScopeFactory = new Mock<IServiceScopeFactory>();
            var logger = new Mock<ILogger<ClimaController>>();
            estadoServicio.Setup(s => s.EstaGenerando()).Returns(false);
            estadoServicio.Setup(s => s.EstaEliminando()).Returns(false);
            vitacoraClimatica.Setup(v => v.Reset())
                .Returns(Task.CompletedTask);

            var climaController = new ClimaController(vitacoraClimatica.Object, serviceScopeFactory.Object,
                estadoServicio.Object, logger.Object);

            var result = climaController.Reset();

            var response = Assert.IsType<OkObjectResult>(result);
            Assert.IsType<string>(response.Value);
        }

        [Fact]
        public async void GetDiaOptimo()
        {
            var estadoServicio = new Mock<IEstadoService>();
            var vitacoraClimatica = new Mock<IBitacoraClimaticaServicio>();
            var serviceScopeFactory = new Mock<IServiceScopeFactory>();
            var logger = new Mock<ILogger<ClimaController>>();
            estadoServicio.Setup(s => s.EstaGenerando()).Returns(false);
            estadoServicio.Setup(s => s.EstaEliminando()).Returns(false);
            vitacoraClimatica.Setup(v => v.GetDiaAsync(1)).ReturnsAsync(new VitacoraClimatica
            {
                Dia = 1,
                Indeterminado = false,
                Lluvia = false,
                Optimo = true,
                Sequia = false,
                PicoLluvias = 1
            });


            var climaController = new ClimaController(vitacoraClimatica.Object, serviceScopeFactory.Object,
                estadoServicio.Object, logger.Object);

            var result = await climaController.GetDia(1);

            var response = Assert.IsType<OkObjectResult>(result);
            var responseValue = Assert.IsType<DiaDto>(response.Value);
            Assert.Equal(1, responseValue.Dia);
            Assert.Equal("Si", responseValue.Optimo);
            Assert.Equal("No", responseValue.Lluvias);
            Assert.Equal("No", responseValue.Sequia);
            Assert.True(responseValue.Determinado);
        }

        [Fact]
        public async void GetDiaLluvias()
        {
            var estadoServicio = new Mock<IEstadoService>();
            var vitacoraClimatica = new Mock<IBitacoraClimaticaServicio>();
            var serviceScopeFactory = new Mock<IServiceScopeFactory>();
            var logger = new Mock<ILogger<ClimaController>>();
            estadoServicio.Setup(s => s.EstaGenerando()).Returns(false);
            estadoServicio.Setup(s => s.EstaEliminando()).Returns(false);
            vitacoraClimatica.Setup(v => v.GetDiaAsync(1)).ReturnsAsync(new VitacoraClimatica
            {
                Dia = 1,
                Indeterminado = false,
                Lluvia = true,
                Optimo = false,
                Sequia = false,
                PicoLluvias = 1
            });


            var climaController = new ClimaController(vitacoraClimatica.Object, serviceScopeFactory.Object,
                estadoServicio.Object, logger.Object);

            var result = await climaController.GetDia(1);

            var response = Assert.IsType<OkObjectResult>(result);
            var responseValue = Assert.IsType<DiaDto>(response.Value);
            Assert.Equal(1, responseValue.Dia);
            Assert.Equal("No", responseValue.Optimo);
            Assert.Equal("Si", responseValue.Lluvias);
            Assert.Equal("No", responseValue.Sequia);
            Assert.True(responseValue.Determinado);
        }

        [Fact]
        public async void GetDiaSequia()
        {
            var estadoServicio = new Mock<IEstadoService>();
            var vitacoraClimatica = new Mock<IBitacoraClimaticaServicio>();
            var serviceScopeFactory = new Mock<IServiceScopeFactory>();
            var logger = new Mock<ILogger<ClimaController>>();
            estadoServicio.Setup(s => s.EstaGenerando()).Returns(false);
            estadoServicio.Setup(s => s.EstaEliminando()).Returns(false);
            vitacoraClimatica.Setup(v => v.GetDiaAsync(1)).ReturnsAsync(new VitacoraClimatica
            {
                Dia = 1,
                Indeterminado = false,
                Lluvia = false,
                Optimo = false,
                Sequia = true,
                PicoLluvias = 1
            });


            var climaController = new ClimaController(vitacoraClimatica.Object, serviceScopeFactory.Object,
                estadoServicio.Object, logger.Object);

            var result = await climaController.GetDia(1);

            var response = Assert.IsType<OkObjectResult>(result);
            var responseValue = Assert.IsType<DiaDto>(response.Value);
            Assert.Equal(1, responseValue.Dia);
            Assert.Equal("No", responseValue.Optimo);
            Assert.Equal("No", responseValue.Lluvias);
            Assert.Equal("Si", responseValue.Sequia);
            Assert.True(responseValue.Determinado);
        }

        [Fact]
        public async void GetDiaIndeterminado()
        {
            var estadoServicio = new Mock<IEstadoService>();
            var vitacoraClimatica = new Mock<IBitacoraClimaticaServicio>();
            var serviceScopeFactory = new Mock<IServiceScopeFactory>();
            var logger = new Mock<ILogger<ClimaController>>();
            estadoServicio.Setup(s => s.EstaGenerando()).Returns(false);
            estadoServicio.Setup(s => s.EstaEliminando()).Returns(false);
            vitacoraClimatica.Setup(v => v.GetDiaAsync(1)).ReturnsAsync(new VitacoraClimatica
            {
                Dia = 1,
                Indeterminado = false,
                Lluvia = false,
                Optimo = false,
                Sequia = false,
                PicoLluvias = 1
            });


            var climaController = new ClimaController(vitacoraClimatica.Object, serviceScopeFactory.Object,
                estadoServicio.Object, logger.Object);

            var result = await climaController.GetDia(1);

            var response = Assert.IsType<OkObjectResult>(result);
            var responseValue = Assert.IsType<DiaDto>(response.Value);
            Assert.Equal(1, responseValue.Dia);
            Assert.Equal("No", responseValue.Optimo);
            Assert.Equal("No", responseValue.Lluvias);
            Assert.Equal("No", responseValue.Sequia);
            Assert.False(responseValue.Determinado);
        }

        [Fact]
        public async void GetDiaNotFound()
        {
            var estadoServicio = new Mock<IEstadoService>();
            var vitacoraClimatica = new Mock<IBitacoraClimaticaServicio>();
            var serviceScopeFactory = new Mock<IServiceScopeFactory>();
            var logger = new Mock<ILogger<ClimaController>>();
            estadoServicio.Setup(s => s.EstaGenerando()).Returns(false);
            estadoServicio.Setup(s => s.EstaEliminando()).Returns(false);
            vitacoraClimatica.Setup(v => v.GetDiaAsync(1)).Returns(Task.FromResult<VitacoraClimatica>(null));


            var climaController = new ClimaController(vitacoraClimatica.Object, serviceScopeFactory.Object,
                estadoServicio.Object, logger.Object);

            var result = await climaController.GetDia(1);

            var response = Assert.IsType<NotFoundObjectResult>(result);
            Assert.IsType<string>(response.Value);
        }
    }
}