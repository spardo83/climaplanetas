using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace ExamenMl.Dominio.Context
{
    /// <summary>
    /// Este factory solo se utiliza en caso que el usuario requiera correr el comando 'dotnet ef database update'
    /// </summary>
    public class GalaxiaDbContextFactory : IDesignTimeDbContextFactory<GalaxiaDbContext>
    {
        public GalaxiaDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<GalaxiaDbContext>();
            optionsBuilder.UseSqlite("Data Source=Galaxia.db", b => b.MigrationsAssembly("ExamenMl.Api"));

            return new GalaxiaDbContext(optionsBuilder.Options);
        }
    }
}