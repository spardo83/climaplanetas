using ExamenMl.Dominio.Entidades;
using Microsoft.EntityFrameworkCore;

namespace ExamenMl.Dominio.Context
{
    /// <summary>
    /// Contexto de entity framework para ORM
    /// </summary>
    public class GalaxiaDbContext : DbContext
    {
        public GalaxiaDbContext(DbContextOptions<GalaxiaDbContext> options)
            : base(options)
        {
        }

        public DbSet<VitacoraClimatica> VitacoraClimaticas { get; set; }
        public DbSet<Planeta> Planetas { get; set; }
    }
}