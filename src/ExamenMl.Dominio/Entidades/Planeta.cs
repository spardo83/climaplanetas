using System.ComponentModel.DataAnnotations;
using ExamenMl.Dominio.Entidades.Enums;

namespace ExamenMl.Dominio.Entidades
{
    public class Planeta
    {
        [Key]
        public long Id { get; set; }
        public string Nombre { get; set; }
        public double DesplazamientoPorDia { get; set; }
        public double GradosDesplazadosPorSegundo { get; set; }
        public int DistanciaAlSol { get; set; }
        public double GradosActual { get; set; }
        public PlanetaEnum EnumValue { get; set; }
    }
}