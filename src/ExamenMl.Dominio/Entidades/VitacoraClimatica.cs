using System.ComponentModel.DataAnnotations;

namespace ExamenMl.Dominio.Entidades
{
    public class VitacoraClimatica
    {
        [Key]
        public long Id { get; set; }
        public long Dia { get; set; }
        public bool Lluvia { get; set; }
        public bool Sequia { get; set; }
        public bool Optimo { get; set; }
        public bool Indeterminado { get; set; }
        public double PicoLluvias { get; set; }
    }
}