namespace ExamenMl.Servicios.Contracts
{
    /// <summary>
    /// Servicio que maneja el estado de la aplicación, debe inyectarse como SINGLETON
    /// </summary>
    public interface IEstadoService
    {
        /// <summary>
        /// Marca como iniciada la generación de datos
        /// </summary>
        /// <param name="cantidadDias">Dias totales a generar</param>
        void ComenzarGeneracionDatos(long cantidadDias);
        /// <summary>
        /// Marca como finalizada la generación de datos
        /// </summary>
        void FinalizarGeneracionDatos();
        /// <summary>
        /// Actualiza los dias generados
        /// </summary>
        void AddDiaGenerado();
        /// <summary>
        /// Obtiene los dias generados actualmente
        /// </summary>
        /// <returns></returns>
        long GetDiasGenerados();
        /// <summary>
        /// Obtiene los dias que se van a generar
        /// </summary>
        /// <returns></returns>
        long GetDiasTotales();
        /// <summary>
        /// Devuelve el estado de la generación de datos
        /// </summary>
        /// <returns></returns>
        bool EstaGenerando();
        /// <summary>
        /// Marca como iniciada la eliminación de datos
        /// </summary>
        void ComenzarEliminacionDeDatos();
        /// <summary>
        /// Marca como finalizada la eliminación de datos
        /// </summary>
        void FinalizarEliminacionDeDatos();
        /// <summary>
        /// Obtiene el estado de la tarea de eliminación
        /// </summary>
        /// <returns></returns>
        bool EstaEliminando();

        /// <summary>
        /// Indica si los planetas deben mantenerse en movimiento
        /// </summary>
        /// <returns></returns>
        bool DebeMoverPlanetas();

        /// <summary>
        /// Marca como iniciado el movimiento de planetas
        /// </summary>
        /// <returns></returns>
        void ComenzarMovimientoPlanetas();
        /// <summary>
        /// Marca como finalizado el movimiento de planetas
        /// </summary>
        /// <returns></returns>
        void FinalizarMovimientoPlanetas();
    }
}