using System.Threading.Tasks;
using ExamenMl.Dominio.Entidades;
using ExamenMl.Servicios.Dto;
using ExamenMl.Servicios.Enums;

namespace ExamenMl.Servicios.Contracts
{
    /// <summary>
    /// Servicio de administración de la bitacora climatica
    /// </summary>
    public interface IBitacoraClimaticaServicio
    {
        /// <summary>
        /// Genera la bitacora de clima
        /// </summary>
        /// <param name="anios">Años a generar</param>
        /// <param name="tipoDesplazamiento">Tipo de desplazamiento solicitado</param>
        /// <returns>Resultados generales</returns>
        Task<ResultadosMeteorologicosDto> GenerarAnios(int anios,
            TipoDesplazamiento tipoDesplazamiento = TipoDesplazamiento.Segundos);

        /// <summary>
        /// Mueve los planetas y envia notificaciones de la posicion para utilizar en front end
        /// </summary>
        /// <param name="tipoDesplazamiento"></param>
        /// <returns></returns>
        Task Movimiento(TipoDesplazamiento tipoDesplazamiento = TipoDesplazamiento.Segundos);
        /// <summary>
        /// Guarda los datos de la bitacora en la base
        /// </summary>
        /// <param name="item">Vitacora a almacenar</param>
        /// <returns></returns>
        Task CreateAsync(VitacoraClimatica item);

        /// <summary>
        /// Obtiene una bitacora para el dia especifico
        /// </summary>
        /// <param name="dia"></param>
        /// <returns></returns>
        Task<VitacoraClimatica> GetDiaAsync(long dia);

        /// <summary>
        /// Verifica si existen los datos iniciales de vitacoras (10 años)
        /// </summary>
        /// <returns></returns>
        Task<bool> ExistenDatosInicialesAsync();

        /// <summary>
        /// Obtiene los resultados almacenados en la base de datos
        /// </summary>
        /// <returns></returns>
        Task<ResultadosMeteorologicosDto> GetResultados();

        /// <summary>
        /// Elimina los resultados de la base de datos
        /// </summary>
        /// <returns></returns>
        Task Reset();

        /// <summary>
        /// Calcula el proximo dia en milisegundos
        /// </summary>
        /// <returns></returns>
        Task CalcularDia();
    }
}