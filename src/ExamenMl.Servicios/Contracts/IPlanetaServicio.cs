using System.Collections.Generic;
using System.Threading.Tasks;
using ExamenMl.Dominio.Entidades;
using ExamenMl.Servicios.Enums;

namespace ExamenMl.Servicios.Contracts
{
    /// <summary>
    /// Servicio de administración de planetas
    /// </summary>
    public interface IPlanetaServicio
    {
        /// <summary>
        /// Desplaza todos los planetas 1 segundo
        /// </summary>
        /// <returns>Devuelve los planetas con su posicion actual</returns>
        void DesplazarTodos(List<Planeta> planetas,TipoDesplazamiento tipoDesplazamiento);


        /// <summary>
        /// Guarda el estado de los planetas despues de años de movimiento
        /// </summary>
        /// <param name="anios">Años de movimiento</param>
        /// <returns></returns>
        Task GuardarEstadoAnios(long anios);
        
        /// <summary>
        /// Guarda el estado de los planetas despues de dias
        /// </summary>
        /// <param name="dias">Dias de movimiento</param>
        /// <returns></returns>
        Task GuardarEstadoDias(long dias);

        /// <summary>
        /// Crea los planetas, opcionalmente se le puede dar una posicion inicial a los tres
        /// </summary>
        /// <param name="posicionInicial">Posicion inicial deseada</param>
        /// <returns></returns>
        Task CreatePlanetasAsync(float? posicionInicial = 0);

        /// <summary>
        /// Devuelve todos los planetas
        /// </summary>
        /// <returns></returns>
        Task<List<Planeta>> GetAll();

        /// <summary>
        /// Redondea la posicion de los planetas para minimizar el error de desplazamiento en el calculo de avance
        /// </summary>
        /// <param name="planetas"></param>
        /// <returns></returns>
        void RedondearAngulos(List<Planeta> planetas);
    }
}