namespace ExamenMl.Servicios.Dto
{
    /// <summary>
    /// Dto de transferencia de resultados
    /// </summary>
    public class ResultadosMeteorologicosDto
    {
        public long PeriodosLLuvia { get; set; }
        public long PeriodosSequia { get; set;}
        public long PeriodosOptimos { get; set;}
        public long PeriodosIndeterminados { get; set; }
        public long DiaMaximaLLuvia { get; set;}

        public ResultadosMeteorologicosDto()
        {
            PeriodosIndeterminados = 0;
            PeriodosOptimos = 0;
            PeriodosSequia = 0;
            PeriodosLLuvia = 0;
            DiaMaximaLLuvia = 0;
        }
    }
}