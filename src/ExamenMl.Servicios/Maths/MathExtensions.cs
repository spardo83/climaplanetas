using System;
using System.Drawing;

namespace ExamenMl.Servicios.Maths
{
    /// <summary>
    /// Clase de utilidades para calculos matematicos de puntos sobre el plano cartesiano y
    /// M.C.U.
    /// </summary>
    public static class MathExtensions
    {
        /// <summary>
        /// Calcula la posición de un punto dado el angulo y el radio
        /// </summary>
        /// <param name="gradosAngulo">Angulo con respecto al centro</param>
        /// <param name="radio">Radio de la circunsferencia</param>
        /// <returns></returns>
        public static Point CalcularPosicion(this double gradosAngulo, int radio)
        {
            return Point.Round(new PointF(radio * (float) Math.Cos(GradosPorRadianes(gradosAngulo)),
                radio * (float) Math.Sin(GradosPorRadianes(gradosAngulo))));
        }

        /// <summary>
        /// Convierte grados angulares a radianes, por definicion las funciones Cos y Sin requieren radianes
        /// https://docs.microsoft.com/en-us/dotnet/api/system.math.cos?redirectedfrom=MSDN&amp;view=netframework-4.8#System_Math_Cos_System_Double_
        /// </summary>
        /// <param name="gradosAngulo">Grados del angulo a convertir</param>
        /// <returns></returns>
        private static double GradosPorRadianes(double gradosAngulo)
        {
            return gradosAngulo * Math.PI / 180.0;
        }

        /// <summary>
        /// Calcula si un punto pertenece a la recta formada por sus extremos
        /// </summary>
        /// <param name="point1">Punto a calcular</param>
        /// <param name="point2">Extremo 1</param>
        /// /// <param name="point3">Extremo 2</param>
        /// <returns></returns>
        public static bool PuntoPerteneceRecta(this Point point1, Point point2, Point point3)
        {
            return (point3.Y - point2.Y) * (point2.X - point1.X) ==
                   (point2.Y - point1.Y) * (point3.X - point2.X);
        }

        /// <summary>
        /// Calcula el perimetro dados tres puntos en el eje cartesiano
        /// </summary>
        /// <param name="p1">Punto 1</param>
        /// <param name="p2">Punto 2</param>
        /// <param name="p3">Punto 3</param>
        /// <returns></returns>
        public static double Perimetro(Point p1, Point p2, Point p3)
        {
            return Math.Round(Math.Sqrt(Math.Pow(p2.X - p1.X, 2) + Math.Pow(p2.Y - p1.Y, 2)) +
                              Math.Sqrt(Math.Pow(p3.X - p2.X, 2) + Math.Pow(p3.Y - p2.Y, 2)) +
                              Math.Sqrt(Math.Pow(p3.X - p1.X, 2) + Math.Pow(p3.Y - p1.Y, 2)));
        }

        /// <summary>
        /// Calcula el area de un triangulo dado por los puntos 
        /// </summary>
        /// <param name="p1">Punto 1</param>
        /// <param name="p2">Punto 2</param>
        /// <param name="p3">Punto 3</param>
        /// <returns></returns>
        public static double Area(Point p1, Point p2, Point p3)
        {
            return Math.Abs((p1.X * (p2.Y - p3.Y) +
                             p2.X * (p3.Y - p1.Y) +
                             p3.X * (p1.Y - p2.Y)) / 2.0);
        }

        /// <summary>
        /// Verifica que un punto evaluado este dentro del triangulo dado por los puntos p1,p2,p3
        /// </summary>
        /// <param name="evaulado"></param>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <returns></returns>
        public static bool EstaDentroDelTriangulo(this Point evaulado, Point p1, Point p2, Point p3)
        {
            // Area del triangulo ABC 
            var abc = Area(p1, p2, p3);

            // Area del triangulo PBC
            var pbc = Area(evaulado, p2, p3);

            // Area del triangulo PAC 
            var pac = Area(p1, evaulado, p3);

            // Area del triangulo PAB 
            var pab = Area(p1, p2, evaulado);

            // Si la suma del area del triangulo PBC PAC y PAB es igual a  el area del triangulo
            // ABC entonces el punto esta dentro del triangulo
            return Math.Abs(abc - (pbc + pac + pab)) < 0.00000000001;
        }

        /// <summary>
        /// Devuleve la cantidad de segundos totales en una cantidad de dias determinada
        /// </summary>
        /// <param name="dias">Dias a calcular</param>
        /// <returns></returns>
        public static long SegundoPorDia(long dias)
        {
            return SegundosPorHora(24) * dias;
        }

        /// <summary>
        /// Devuleve la cantidad de segundos totales en una cantidad de dias determinada
        /// </summary>
        /// <param name="horas">Horas a calcular</param>
        /// <returns></returns>
        public static long SegundosPorHora(long horas)
        {
            return 3600 * horas;
        }

        /// <summary>
        /// Devuelve la cantidad de dias totales en una cantidad de años determinada
        /// </summary>
        /// <param name="anios">Los años a calcular</param>
        /// <returns></returns>
        public static long DiasPorAnios(long anios)
        {
            return 365 * anios;
        }
    }
}