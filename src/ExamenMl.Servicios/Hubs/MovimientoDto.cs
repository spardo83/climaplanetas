namespace ExamenMl.Servicios.Hubs
{
    /// <summary>
    /// Dto de transferencia de datos de movimiento
    /// </summary>
    public class MovimientoDto
    {
        public string Nombre { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public bool Sequia { get; set; }
        public bool Optimo { get; set; }
        public bool Lluvia { get; set; }
        public double AnguloPlaneta { get; set; }
    }
}