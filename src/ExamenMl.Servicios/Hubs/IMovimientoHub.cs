using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExamenMl.Servicios.Hubs
{
    /// <summary>
    /// Interfaz para tipar el hub de movimiento
    /// </summary>
    public interface IMovimientoHub
    {
        Task Movimiento(List<MovimientoDto> movimiento);
    }
}