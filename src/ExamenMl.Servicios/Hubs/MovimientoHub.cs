using Microsoft.AspNetCore.SignalR;

namespace ExamenMl.Servicios.Hubs
{
    /// <summary>
    /// Hub de signalR (parecido a stomp) que permite notificar a un cliente o varios
    /// </summary>
    public class MovimientoHub : Hub<IMovimientoHub>
    {
        
    }
}