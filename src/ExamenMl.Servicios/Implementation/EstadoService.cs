using ExamenMl.Servicios.Contracts;

namespace ExamenMl.Servicios.Implementation
{
    public class EstadoService : IEstadoService
    {
        private bool _generando = false;
        private long _cantidadDias = 0;
        private long _currentDia = 0;
        private bool _eliminando = false;
        private bool _moverPlanetas = false;

        public void ComenzarGeneracionDatos(long cantidadDias)
        {
            _cantidadDias = cantidadDias;
            _generando = true;
        }

        public void FinalizarGeneracionDatos()
        {
            _generando = false;
            _cantidadDias = 0;
            _currentDia = 0;
        }

        public void AddDiaGenerado()
        {
            _currentDia += 1;
        }

        public long GetDiasGenerados()
        {
            return _currentDia;
        }

        public long GetDiasTotales()
        {
            return _cantidadDias;
        }

        public bool EstaGenerando()
        {
            return _generando;
        }

        public void ComenzarEliminacionDeDatos()
        {
            _eliminando = true;
        }

        public void FinalizarEliminacionDeDatos()
        {
            _eliminando = false;
        }

        public bool EstaEliminando()
        {
            return _eliminando;
        }

        public bool DebeMoverPlanetas()
        {
            return _moverPlanetas;
        }

        public void ComenzarMovimientoPlanetas()
        {
            _moverPlanetas = true;
        }

        public void FinalizarMovimientoPlanetas()
        {
            _moverPlanetas = false;
        }
    }
}