using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ExamenMl.Dominio.Context;
using ExamenMl.Dominio.Entidades;
using ExamenMl.Dominio.Entidades.Enums;
using ExamenMl.Servicios.Contracts;
using ExamenMl.Servicios.Enums;
using ExamenMl.Servicios.Maths;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace ExamenMl.Servicios.Implementation
{
    public class PlanetaServicio : IPlanetaServicio
    {
        private readonly GalaxiaDbContext _dbContext;
        private const float SegundosEnUnDia = 86400;
        private readonly ILogger<PlanetaServicio> _logger;

        public PlanetaServicio(GalaxiaDbContext dbContext, ILogger<PlanetaServicio> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }


        public void DesplazarTodos(List<Planeta> planetas, TipoDesplazamiento tipoDesplazamiento)
        {
            try
            {
                foreach (var planeta in planetas)
                {
                    switch (tipoDesplazamiento)
                    {
                        case TipoDesplazamiento.Segundos:
                            planeta.GradosActual += planeta.GradosDesplazadosPorSegundo;
                            break;
                        case TipoDesplazamiento.Dias:
                            planeta.GradosActual += planeta.DesplazamientoPorDia;
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error desplazando los planetas");
            }
        }

        public async Task GuardarEstadoAnios(long anios)
        {
            try
            {
                foreach (var planeta in _dbContext.Planetas)
                {
                    planeta.GradosActual = planeta.GradosActual +
                                           planeta.DesplazamientoPorDia * MathExtensions.DiasPorAnios(anios);
                    _dbContext.Update(planeta);
                }

                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error guardando el estado de los planetas en años: {anios}", anios);
            }
        }

        public async Task GuardarEstadoDias(long dias)
        {
            try
            {
                foreach (var planeta in _dbContext.Planetas)
                {
                    planeta.GradosActual = planeta.GradosActual +
                                           planeta.DesplazamientoPorDia * dias;
                    _dbContext.Update(planeta);
                }

                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error guardando el estado de los planetas en dias:", dias);
            }
        }

        public async Task CreatePlanetasAsync(float? posicionInicial = 0)
        {
            try
            {
                if (await _dbContext.Planetas.AnyAsync()) return;
                await _dbContext.AddAsync(new Planeta
                {
                    Nombre = "Ferengi",
                    DistanciaAlSol = 500,
                    // Si no recibe una posicion inicial lo setea en 0
                    GradosActual = posicionInicial ?? 0,
                    DesplazamientoPorDia = -1,
                    GradosDesplazadosPorSegundo = -1 / SegundosEnUnDia,
                    EnumValue = PlanetaEnum.Ferengi
                });
                await _dbContext.AddAsync(new Planeta
                {
                    Nombre = "Betasoide",
                    DistanciaAlSol = 2000,
                    // Si no recibe una posicion inicial lo setea en 0
                    GradosActual = posicionInicial ?? 0,
                    DesplazamientoPorDia = -3,
                    GradosDesplazadosPorSegundo = -3 / SegundosEnUnDia,
                    EnumValue = PlanetaEnum.Betasoide
                });
                await _dbContext.AddAsync(new Planeta
                {
                    Nombre = "Vulcano",
                    DistanciaAlSol = 1000,
                    // Si no recibe una posicion inicial lo setea en 0
                    GradosActual = posicionInicial ?? 0,
                    DesplazamientoPorDia = 5,
                    GradosDesplazadosPorSegundo = 5 / SegundosEnUnDia,
                    EnumValue = PlanetaEnum.Vulcano
                });
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error durante la creación de los planetas");
            }
        }

        public async Task<List<Planeta>> GetAll()
        {
            return await _dbContext.Planetas.ToListAsync();
        }

        public void RedondearAngulos(List<Planeta> planetas)
        {
            foreach (var planeta in planetas)
            {
                planeta.GradosActual = Math.Round(planeta.GradosActual);
            }
        }
    }
}