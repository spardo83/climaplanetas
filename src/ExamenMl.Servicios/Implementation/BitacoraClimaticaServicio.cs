using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using ExamenMl.Dominio.Context;
using ExamenMl.Dominio.Entidades;
using ExamenMl.Dominio.Entidades.Enums;
using ExamenMl.Servicios.Contracts;
using ExamenMl.Servicios.Dto;
using ExamenMl.Servicios.Enums;
using ExamenMl.Servicios.Hubs;
using ExamenMl.Servicios.Maths;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace ExamenMl.Servicios.Implementation
{
    public class BitacoraClimaticaServicio : IBitacoraClimaticaServicio
    {
        private readonly GalaxiaDbContext _dbContext;
        private readonly IPlanetaServicio _planetaServicio;
        private readonly IHubContext<MovimientoHub, IMovimientoHub> _hub;
        private readonly IEstadoService _estadoService;
        private readonly ILogger<BitacoraClimaticaServicio> _logger;

        public BitacoraClimaticaServicio(GalaxiaDbContext dbContext, IPlanetaServicio planetaServicio,
            IHubContext<MovimientoHub, IMovimientoHub> hub, IEstadoService estadoService,
            ILogger<BitacoraClimaticaServicio> logger)
        {
            _dbContext = dbContext;
            _planetaServicio = planetaServicio;
            _hub = hub;
            _estadoService = estadoService;
            _logger = logger;
        }

        public async Task CreateAsync(VitacoraClimatica item)
        {
            try
            {
                await _dbContext.Set<VitacoraClimatica>().AddAsync(item);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error al grabar el item {item}", item);
            }
        }

        public async Task<VitacoraClimatica> GetDiaAsync(long dia)
        {
            return await _dbContext.VitacoraClimaticas.Where(x => x.Dia == dia).FirstOrDefaultAsync();
        }

        public async Task<bool> ExistenDatosInicialesAsync()
        {
            return await _dbContext.VitacoraClimaticas.AnyAsync();
        }

        public async Task<ResultadosMeteorologicosDto> GetResultados()
        {
            try
            {
                var diasLluvia = await _dbContext.VitacoraClimaticas.Where(x => x.Lluvia).CountAsync();
                var diasSequia = await _dbContext.VitacoraClimaticas.Where(x => x.Sequia).CountAsync();
                var diasOptimos = await _dbContext.VitacoraClimaticas.Where(x => x.Optimo).CountAsync();
                var diasIndeterminados = await _dbContext.VitacoraClimaticas.Where(x => x.Indeterminado).CountAsync();
                var diaMaximaLLuvia = await _dbContext.VitacoraClimaticas.OrderByDescending(x => x.PicoLluvias)
                    .Select(x => x.Dia).FirstAsync();
                return new ResultadosMeteorologicosDto
                {
                    PeriodosIndeterminados = diasIndeterminados,
                    PeriodosOptimos = diasOptimos,
                    PeriodosSequia = diasSequia,
                    PeriodosLLuvia = diasLluvia,
                    DiaMaximaLLuvia = diaMaximaLLuvia
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error obteniendo los resultados GetResultados()");
                return new ResultadosMeteorologicosDto();
            }
        }

        public async Task Reset()
        {
            _estadoService.ComenzarEliminacionDeDatos();
            _dbContext.VitacoraClimaticas.RemoveRange(_dbContext.VitacoraClimaticas);
            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error guarando los cambios de eliminación");
            }

            _estadoService.FinalizarEliminacionDeDatos();
        }

        public async Task CalcularDia()
        {
            // Si no hay datos inicializados no realiza la accion
            if (!await ExistenDatosInicialesAsync()) return;

            var ultimoDia = await _dbContext.VitacoraClimaticas.OrderByDescending(x => x.Dia).FirstOrDefaultAsync();
            var planetas = await _planetaServicio.GetAll();
            var bitacora = new VitacoraClimatica
            {
                Dia = ultimoDia.Dia + 1
            };
            // For parallelo en multiples hilos que permite optimizar el tiempo de calculo
            Parallel.For(0, MathExtensions.SegundoPorDia(1),
                new ParallelOptions {MaxDegreeOfParallelism = Environment.ProcessorCount}, s =>
                {
                    try
                    {
                        CalcularVitacora(planetas, bitacora, s + MathExtensions.SegundoPorDia(bitacora.Dia),
                            TipoDesplazamiento.Segundos);
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e, "Error durante el calculo de la bitacora en el segundo {s} del dia {dia}",
                            s, bitacora.Dia);
                    }
                });

            _planetaServicio.RedondearAngulos(planetas);
            await _dbContext.VitacoraClimaticas.AddAsync(bitacora);
            await _planetaServicio.GuardarEstadoDias(1);
            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error al guardar el estado de los planetas");
            }
        }

        public async Task<ResultadosMeteorologicosDto> GenerarAnios(int anios,
            TipoDesplazamiento tipoDesplazamiento = TipoDesplazamiento.Segundos)
        {
            // Si los datos existen se devuelven sus resultados
            if (await ExistenDatosInicialesAsync()) return await GetResultados();

            _estadoService.ComenzarGeneracionDatos(MathExtensions.DiasPorAnios(anios));

            var planetas = await _planetaServicio.GetAll();
            var vitacoras = new List<VitacoraClimatica>();

            // Se avanza de a un dia, este caso no es paralelizable ya que Entity Framework no es Thread Safe
            for (var dia = 0L; dia < MathExtensions.DiasPorAnios(anios); dia++)
            {
                var bitacora = new VitacoraClimatica {Dia = dia, PicoLluvias = 0};

                //Seteamos el multiplicador y el maximo del for en su default que es por dia 
                var multiplicadorParallel = dia;
                var forMax = 1L;

                // Calculamos el multiplicador para el parallel for, esto nos permitira que indistintamente
                // de cuando se ejecute el hilo calcule el tiempo para el el dia que corresponde y el segundo que corresponde
                // solo usamos casos de segundos o milisegundos porque seteamos dia por defecto


                if (tipoDesplazamiento == TipoDesplazamiento.Segundos)
                {
                    multiplicadorParallel = MathExtensions.SegundoPorDia(dia);
                    forMax = MathExtensions.SegundoPorDia(1);
                }


                // For parallelo en multiples hilos que permite optimizar el tiempo de calculo
                var diaParallel = dia;
                Parallel.For(0, forMax,
                    new ParallelOptions {MaxDegreeOfParallelism = Environment.ProcessorCount}, s =>
                    {
                        try
                        {
                            CalcularVitacora(planetas, bitacora, s + multiplicadorParallel,
                                tipoDesplazamiento);
                        }
                        catch (Exception e)
                        {
                            _logger.LogError(e, "Error durante el calculo de la bitacora segundo {s} dia {dia}", s,
                                diaParallel);
                        }
                    });

                // Si no se dio ningun fenomeno se guarda en la bitacora
                if (!bitacora.Sequia && !bitacora.Lluvia && !bitacora.Optimo)
                {
                    bitacora.Indeterminado = true;
                }

                // Si no existen datos generados se guarda una bitacora del clima para cada dia, evaluando si existieron periodos de
                // lluvia,optimos,sequia o indeterminados. Guarda de a pedazos de 1000 dias para mejorar la performance del guardado
                vitacoras.Add(bitacora);
                _estadoService.AddDiaGenerado();
                if (dia % 1000 == 0)
                {
                    try
                    {
                        await _dbContext.AddRangeAsync(vitacoras);
                        await _dbContext.SaveChangesAsync();
                        DetachAllEntities();
                        vitacoras.Clear();
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e, "Error guardando los datos dias {desde} - {hasta}", dia - 1000, dia);
                    }
                }
            }

            // Guarda la ultima parte que no fue guardada
            try
            {
                _estadoService.FinalizarGeneracionDatos();
                await _dbContext.AddRangeAsync(vitacoras);
                await _dbContext.SaveChangesAsync();
                DetachAllEntities();
                vitacoras.Clear();
            }
            catch (Exception e)
            {
                _logger.LogError(e,"Error guardando el ultimo set de datos");
            }
            

            // Guardamos el estado de los planetas despues del calculo
            await _planetaServicio.GuardarEstadoAnios(anios);

            return await GetResultados();
        }


        /// <summary>
        /// Mueve los planetas notificando a una interfaz grafica
        /// </summary>
        /// <param name="tipoDesplazamiento">Tipo de desplazamiento</param>
        public async Task Movimiento(TipoDesplazamiento tipoDesplazamiento = TipoDesplazamiento.Segundos)
        {
            _estadoService.ComenzarMovimientoPlanetas();

            while (_estadoService.DebeMoverPlanetas())
            {
                var planetas = await _planetaServicio.GetAll();
                var bitacora = new VitacoraClimatica();
                var ferengi = planetas.Single(x => x.EnumValue == PlanetaEnum.Ferengi);
                var posicionFerengi = ferengi.GradosActual.CalcularPosicion(ferengi.DistanciaAlSol);

                var betasoide = planetas.Single(x => x.EnumValue == PlanetaEnum.Betasoide);
                var posicionBetasoide = betasoide.GradosActual.CalcularPosicion(betasoide.DistanciaAlSol);

                var vulcano = planetas.Single(x => x.EnumValue == PlanetaEnum.Vulcano);
                var posicionVulcano = vulcano.GradosActual.CalcularPosicion(vulcano.DistanciaAlSol);

                if (Sequia(posicionVulcano, posicionFerengi, posicionBetasoide))
                {
                    bitacora.Sequia = true;
                }
                else if (Optima(posicionVulcano, posicionFerengi, posicionBetasoide))
                {
                    bitacora.Optimo = true;
                }
                else if (Lluvia(posicionVulcano, posicionFerengi, posicionBetasoide))
                {
                    bitacora.Lluvia = true;
                    // Calculamos el permitero que forman los planetas, ya que podemos asumir que hubo lluvias
                    var perimetro = MathExtensions.Perimetro(posicionVulcano, posicionBetasoide,
                        posicionFerengi);

                    // En caso que el perimetro supere al pico maximo almacenamos ese valor que sera utilizado para este dia
                    if (bitacora.PicoLluvias < perimetro) bitacora.PicoLluvias = perimetro;
                }

                //Notifica a los clientes la posicion actual de los planetas, podria se usado en una UI 
                await _hub.Clients.All.Movimiento(new List<MovimientoDto>
                {
                    new MovimientoDto
                    {
                        Nombre = "Ferengi", X = posicionFerengi.X, Y = posicionFerengi.Y, Lluvia = bitacora.Lluvia,
                        Sequia = bitacora.Sequia, Optimo = bitacora.Optimo, AnguloPlaneta = ferengi.GradosActual
                    },
                    new MovimientoDto
                    {
                        Nombre = "Betasoide", X = posicionBetasoide.X, Y = posicionBetasoide.Y,
                        Lluvia = bitacora.Lluvia,
                        Sequia = bitacora.Sequia, Optimo = bitacora.Optimo, AnguloPlaneta = betasoide.GradosActual
                    },
                    new MovimientoDto
                    {
                        Nombre = "Vulcano", X = posicionVulcano.X, Y = posicionVulcano.Y,
                        Lluvia = bitacora.Lluvia,
                        Sequia = bitacora.Sequia, Optimo = bitacora.Optimo, AnguloPlaneta = vulcano.GradosActual
                    }
                });
                //Desplazamos los planetas
                _planetaServicio.DesplazarTodos(planetas, tipoDesplazamiento);
            }
        }

        /// <summary>
        /// Calcula la bitacora de una lista de planetas en un instante determinado
        /// </summary>
        /// <param name="planetas">Planetas a calcular</param>
        /// <param name="bitacora">Vitacora por referencia</param>
        /// <param name="multiplicador">Multiplicador de posicion para determinar el instante</param>
        /// <param name="tipoDesplazamiento">Tipo de desplazamiento para el calculo de posicion</param>
        private void CalcularVitacora(IReadOnlyCollection<Planeta> planetas, VitacoraClimatica bitacora,
            long multiplicador,
            TipoDesplazamiento tipoDesplazamiento)
        {
            var ferengi = planetas.Single(x => x.EnumValue == PlanetaEnum.Ferengi);
            var betasoide = planetas.Single(x => x.EnumValue == PlanetaEnum.Betasoide);
            var vulcano = planetas.Single(x => x.EnumValue == PlanetaEnum.Vulcano);
            var posicionFerengi = PosicionPlaneta(ferengi, multiplicador, tipoDesplazamiento);
            var posicionBetasoide = PosicionPlaneta(betasoide, multiplicador, tipoDesplazamiento);
            var posicionVulcano = PosicionPlaneta(vulcano, multiplicador, tipoDesplazamiento);

            if (Sequia(posicionVulcano, posicionFerengi, posicionBetasoide))
            {
                bitacora.Sequia = true;
            }
            else if (Optima(posicionVulcano, posicionFerengi, posicionBetasoide))
            {
                bitacora.Optimo = true;
            }
            else if (Lluvia(posicionVulcano, posicionFerengi, posicionBetasoide))
            {
                bitacora.Lluvia = true;
                // Calculamos el permitero que forman los planetas, ya que podemos asumir que hubo lluvias
                var perimetro = MathExtensions.Perimetro(posicionVulcano, posicionBetasoide,
                    posicionFerengi);

                // En caso que el perimetro supere al pico maximo almacenamos ese valor que sera utilizado para este dia
                if (bitacora.PicoLluvias < perimetro) bitacora.PicoLluvias = perimetro;
            }
        }

        /// <summary>
        /// Calcula la posicion de un planeta en el eje cartesiano para un momento especifico
        /// </summary>
        /// <param name="planeta">Plnaeta a calcular</param>
        /// <param name="multiplicador">Multiplicador de desplazamiento</param>
        /// <param name="tipoDesplazamiento">Tipo de desplazamiento a calcular</param>
        /// <returns></returns>
        private Point PosicionPlaneta(Planeta planeta, long multiplicador, TipoDesplazamiento tipoDesplazamiento)
        {
            var posicion = new Point(0, 0);

            switch (tipoDesplazamiento)
            {
                case TipoDesplazamiento.Dias:
                    posicion =
                        (planeta.DesplazamientoPorDia * multiplicador).CalcularPosicion(planeta.DistanciaAlSol);

                    break;
                case TipoDesplazamiento.Segundos:
                    posicion =
                        (planeta.GradosDesplazadosPorSegundo * multiplicador).CalcularPosicion(planeta.DistanciaAlSol);
                    break;
            }

            return posicion;
        }

        /// <summary>
        /// Utilizando una funcion que permite calcular si un punto pertenece a la recta que forman otros dos,
        /// Verificamos que Vulcano y el Sol pertenescan a la recta formada por Ferengi y Betasoide
        /// </summary>
        /// <param name="posicionVulcano">Vulcano</param>
        /// <param name="posicionFerengi">Ferengi</param>
        /// <param name="posicionBetasoide">Betasoide</param>
        /// <returns></returns>
        private bool Sequia(Point posicionVulcano, Point posicionFerengi, Point posicionBetasoide)
        {
            return posicionVulcano.PuntoPerteneceRecta(posicionFerengi, posicionBetasoide) &&
                   new Point(0, 0).PuntoPerteneceRecta(posicionFerengi, posicionBetasoide);
        }

        /// <summary>
        /// Utilizando la funcion verificacmos si el sol se encuentra dentro del triangulo
        /// </summary>
        /// <param name="posicionVulcano">Vulcano</param>
        /// <param name="posicionFerengi">Ferengi</param>
        /// <param name="posicionBetasoide">Betasoide</param>
        /// <returns></returns>
        private bool Lluvia(Point posicionVulcano, Point posicionFerengi, Point posicionBetasoide)
        {
            return new Point(0, 0).EstaDentroDelTriangulo(posicionFerengi, posicionBetasoide,
                posicionVulcano);
        }

        /// <summary>
        ///Utilizando una funcion que permite comparar si un punto pertenece a la recta formada por otros dos,
        /// primero verificamos que no sea un dia de sequia, ya que eso indicaria que el sol tambien esta alineado
        /// y luego verificamos si el planeta Vulcano esta en la misma recta que los extremos Ferengi y Betasoide
        /// </summary>
        /// <param name="posicionVulcano">Vulcano</param>
        /// <param name="posicionFerengi">Ferengi</param>
        /// <param name="posicionBetasoide">Betasoide</param>
        /// <returns></returns>
        private bool Optima(Point posicionVulcano, Point posicionFerengi, Point posicionBetasoide)
        {
            return posicionVulcano.PuntoPerteneceRecta(posicionFerengi, posicionBetasoide);
        }

        /// <summary>
        /// Desatachea las entidades unidas al dbcontext, esto permite mejorar la performance al guardar
        /// </summary>
        private void DetachAllEntities()
        {
            var changedEntriesCopy = _dbContext.ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Added ||
                            e.State == EntityState.Modified ||
                            e.State == EntityState.Deleted)
                .ToList();

            foreach (var entry in changedEntriesCopy)
                entry.State = EntityState.Detached;
        }
    }
}